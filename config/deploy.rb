set :application, "toutiaoarticle"
set :repo_url, "git@gitlab.com:jackbu/toutiaoarticle.git"

set :deploy_to, '/home/ubuntu/toutiaoarticle'

append :linked_files, "config/database.yml", "config/secrets.yml"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "vendor/bundle", "public/system", "public/uploads"